import net.szaboferee.AlternativeSolution;
import net.szaboferee.Solution;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DynamicSolutionTest {

    @TestFactory
    List<DynamicTest> createTests() {

        List<ProblemConfiguration> problems = collectSolutionConfigs();

        return generateTestCases(
                problems.stream().filter(problem -> {
                    if(System.getenv().containsKey("runOnly")) {
                        return System.getenv().get("runOnly").contains(problem.problemName);
                    } else {
                        return true;
                    }
                }).collect(Collectors.toList())
        );
    }

    private List<DynamicTest> generateTestCases(List<ProblemConfiguration> solutionConfigs) {
        return solutionConfigs.stream()
                .map(this::createTestCaseFromConfig)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<ProblemConfiguration> collectSolutionConfigs() {
        ArrayList<ProblemConfiguration> problems = new ArrayList<>();
        try {
            Files.newDirectoryStream(new File("src/test/resources/problems").toPath()).forEach(
                    problemDir -> {
                        ArrayList<TestCase> testcases = new ArrayList<>();
                        problems.add(new ProblemConfiguration(problemDir.getFileName().toString(), testcases));
                        try {
                            Files.newDirectoryStream(problemDir, "input*.txt").forEach(path -> {
                                try {
                                    String input = new String(Files.readAllBytes(path));
                                    File outputFile = new File(path.toAbsolutePath().toString().replace("in", "out"));
                                    if (!outputFile.exists()) {
                                        outputFile.createNewFile();
                                    }
                                    String output = new String(Files.readAllBytes(outputFile.toPath().toAbsolutePath()));
                                    testcases.add(new TestCase(path.getFileName().toString(), input, output));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return problems;
    }

    private List<DynamicTest> createTestCaseFromConfig(ProblemConfiguration problemConfiguration) {
        String problemName = problemConfiguration.problemName;
            try {
                String className = String.format("net.szaboferee.solutions.Solution%s", problemName);
                System.out.println("Check if class " + className + " exists");
                Class<?> classUnderTest = Class.forName(className);
                System.out.println(className + " found. collecting testcases");
                System.out.println();

                return problemConfiguration.testcases.stream().map(testCase -> {
                    List<DynamicTest> tests = new ArrayList<>();
                        tests.add(DynamicTest.dynamicTest("Test for problem " + problemName + " with " + testCase.inputName, () -> {
                            //solve with default
                            String actual = ((Solution) classUnderTest.newInstance()).solveForInput(testCase.input);
                            String expected = testCase.output;
                            assertEquals(expected, actual);
                        }));

                        if (classUnderTest.isAssignableFrom(AlternativeSolution.class)) {
                            System.out.println(className + " has alternative solution ");
                            tests.add(DynamicTest.dynamicTest("Test for problem " + problemName + " using alternative solution with " + testCase.inputName, () -> {
                                //solve with alternative
                                String actual = ((AlternativeSolution) classUnderTest.newInstance()).solveForInputAlternative(testCase.input);
                                String expected = testCase.output;
                                assertEquals(expected, actual);
                            }));
                        }

                    return tests;
                }).flatMap(Collection::stream).collect(Collectors.toList());

            } catch (ClassNotFoundException e) {
                //no solution implemented yet
                System.out.println(String.format("There is no solution yet for problem %s", problemName));
            }
        return Collections.emptyList();
    }

}
