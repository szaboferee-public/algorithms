import java.util.List;

public class ProblemConfiguration {
    public String problemName;
    public List<TestCase> testcases;

    public ProblemConfiguration(String solutionClassName, List<TestCase> testcases) {
        this.problemName = solutionClassName;
        this.testcases = testcases;
    }
}
