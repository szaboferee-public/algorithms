public class TestCase {
    public String inputName;
    public String input;
    public String output;

    public TestCase(String inputName, String input, String output) {
        this.inputName = inputName;
        this.input = input;
        this.output = output;
    }
}
