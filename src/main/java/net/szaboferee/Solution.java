package net.szaboferee;

public interface Solution {
    String solveForInput(String input);
}
