package net.szaboferee;

public interface AlternativeSolution {
    String solveForInputAlternative(String input);
}
