package net.szaboferee.solutions;

import net.szaboferee.Solution;

import java.util.*;
import java.util.stream.Collectors;

public class Solution26 implements Solution {
    private static final int WHITE = 0;
    private static final int GREY = 1;
    private static final int BLACK = 2;
    private int testCounter = 1;

    @Override
    public String solveForInput(String input) {
        String output = "";
        String[] lines = input.split("\n");
        int currentLine = 0;
        int N;
        N = Integer.parseInt(lines[currentLine++]);
        while (N > 0) {
            Graph g = new Graph();
            String[] parts = lines[currentLine++].split(" ");
            String[] labels = Arrays.copyOfRange(parts, 0, 2*N);
            String[] tests =  Arrays.copyOfRange(parts, 2*N, parts.length);
            for (int i = 0; i < 2*N; i+=2) {
                g.add(labels[i], labels[i+1]);
            }

            for (int i = 0; !"0".equals(tests[i]) && !"0".equals(tests[i + 1]); i += 2) {
                output += processGraph(g, tests[i], Integer.parseInt(tests[i+1]));
            }

            N = Integer.parseInt(lines[currentLine++]);
        }


        return output;
    }

    private String processGraph(Graph g, String label, int ttl) {
        String output = "";
        Node node = g.nodeMap.get(label);
        g.reset();
        Queue<Node> Q = new ArrayDeque<>();
        node.distance = 0;
        node.color = GREY;
        Q.add(node);
        boolean end = false;
        while (!end && !Q.isEmpty()) {
            Node u = Q.poll();
            u.color = BLACK;
            end = ttl == u.distance;
            if (end) break;

            u.connections.stream().filter(v -> v.color == WHITE).forEach(v -> {
                v.color = GREY;
                v.distance = u.distance+1;
                Q.add(v);
            });
        }

        long count = g.nodeMap.values().stream().filter(n -> n.color == WHITE).count();
        output += "Teszt #" + testCounter++ + ": " + count + " csúcs nem érhető el a " + label + " csúcsból TTL=" + ttl + " értékkel.\n";
        return output;
    }

    private class Graph {
        public Map<String, Node> nodeMap = new HashMap<>();

        public void add(String label1, String label2) {
            Node node1 = nodeMap.computeIfAbsent(label1, Node::new);
            Node node2 = nodeMap.computeIfAbsent(label2, Node::new);
            node1.add(node2);
            node2.add(node1);
        }

        public void reset() {
            nodeMap.values().forEach(Node::reset);
        }
    }

    private class Node {
        public String label;
        public List<Node> connections = new ArrayList<>();
        public int distance = Integer.MAX_VALUE;
        public int color = WHITE;

        public Node(String label) {
            this.label = label;
        }

        public void add(Node node) {
            if (connections.contains(node)) {
                return;
            }
            connections.add(node);
        }


        public void reset() {
            distance = Integer.MAX_VALUE;
            color = WHITE;
        }

        @Override
        public String toString() {
            return label + ": " + connections.stream().map(node -> node.label).collect(Collectors.joining(","));
        }
    }
}
